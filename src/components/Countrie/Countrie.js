import React from 'react';

const Countrie = props => {
    return (
        <div onClick={props.clicked}>
            <h1>{props.title}</h1>
        </div>
    );
};

export default Countrie;