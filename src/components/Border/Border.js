import React from 'react';

const Border = props => {
    return (
        <li> {props.border} </li>
    );
};

export default Border;