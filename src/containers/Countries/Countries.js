import React, {useState, useEffect} from 'react';
import Countrie from '../.././components/Countrie/Countrie';
import FullCountry from '../FullCountry/FullCountry';
import axios from 'axios';

const BASE_URL = 'https://restcountries.com/v2/all';

const Countries = () => {
    const [countries, setCountries] = useState([]);
    const [selectedCountry, setSelectedCountry] = useState(null);

    useEffect(() => {
        const fetchData = async () => {
            const countriesResponse = await axios.get(BASE_URL);
            const promises = countriesResponse.data.map(async country => {
                return {name: country.name};
            });
            const newCountries = await Promise.all(promises);
            setCountries(newCountries);
        };
        fetchData().catch(console.error);
    }, []);
    console.log(selectedCountry);

    return (
        <div className="flex-container ">
            <section className="layer">
                {countries.map(country => (
                    <Countrie key={country.name}
                              title={country.name}
                              clicked={() => setSelectedCountry(country.name)}
                    />
                ))}
            </section>
            <section>
                <FullCountry name={selectedCountry}
                />
            </section>
        </div>
    );
};

export default Countries;