import React, {useCallback, useEffect, useState} from 'react';
import axios from 'axios';
import CountrieCard from "../CountrieCard/CountrieCard";

const FullCountry = props => {
    const [countryInfo, setCountryInfo] = useState(null);
    const countryName_url = "https://restcountries.eu/rest/v2/name/" + props.name
    const fetchData = useCallback(async () => {
        if (props.name !== null) {
            const countryInfoResponse = await axios.get(countryName_url);
            setCountryInfo(countryInfoResponse.data);
        }
    }, [props.name]);

    useEffect(() => {
        fetchData().catch(console.error);
    }, [props.name]);

    let arr = [];
    if (countryInfo !== null) {
        countryInfo.forEach(function (element) {
            arr.push(<CountrieCard key={element.name}
                                   population={element.population}
                                   capital={element.capital}
                                   borders={element.borders}
                                   flag={element.flag}/>)
        });
    };

    return countryInfo && (
        <div>
            <h1 className="country">
                {props.name}
            </h1>
            {arr}
        </div>
    );
};

export default FullCountry;