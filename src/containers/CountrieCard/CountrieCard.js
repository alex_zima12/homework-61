import React, {useEffect, useState} from 'react';
import Border from '../../components/Border/Border';
import axios from "axios";

const BORDER_URL = 'https://restcountries.eu/rest/v2/alpha/'

const CountrieCard = props => {
    const [borders, setBorders] = useState(null);

    useEffect(() => {
        const fetchData = async () => {
            const promises = props.borders.map(async element => {
                const bordersResponse = await axios.get(BORDER_URL+element);
                return {...borders, border: bordersResponse.data.name};
            });
            const newBorders = await Promise.all(promises);
            setBorders(newBorders)
        }; fetchData().catch(console.error);
    }, [props.borders]);

    let arr = [];
    if (borders !== null && borders.length>0) {
     borders.forEach(function (element){
             arr.push(<Border border = {element.border}></Border>)} )
    } else {
        arr.push(<p> Страна не имеет границ</p> )
    }

    return borders && (
        <div className="countryCard">
            <img src={props.flag} alt="flag" style={{ width: 100}} />
            <p>Столица: {props.capital}</p>
            <p>Численность населения: {props.population}</p>
            <p>Границы:</p>
            <ul>{arr}</ul>
        </div>
    );

};

export default CountrieCard;