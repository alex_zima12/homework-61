import React from 'react';
import './App.css';
import Countries from './containers/Countries/Countries';

function App() {
    return (
        <div className="App">
            <Countries></Countries>
        </div>
    );
}

export default App;
